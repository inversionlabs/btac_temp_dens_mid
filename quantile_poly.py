#!/usr/bin/env python

# Patrick Wright w/assistance from Josef Perktold
# Inversion Labs
# Feb. 2016


import numpy as np
import scipy as sp
from scipy import stats
import scipy.stats as stats
import pylab 
import pandas as pd
from IPython import embed
import patsy
import statsmodels.api as sm
import statsmodels.formula.api as smf
import matplotlib.pyplot as plt
import statsmodels.formula.api as smf

print "Processing datafiles..."

# Define directory paths:
fpath_1='dbo.HistoricalWeatherTable_20160103.csv' # historical data
fpath_2='dbo.Teton_AM_Forecast_Data.csv' # temps since 2002

# Read in data:
data1 = pd.read_csv(fpath_1, sep=',')
data2 = pd.read_csv(fpath_2, sep=',')

# Drop last entry of data1 (NaN for date, no data)
data1 = data1[:-1]

# Convert datetime strings to a Pandas datetime object
timestamp1 = data1['fldDate']
dt1 = pd.to_datetime(timestamp1, infer_datetime_format=True)
timestamp2 = data2['fldDate']
dt2 = pd.to_datetime(timestamp2, infer_datetime_format=True)

# set index of dataframes to datetime
data1_dt = data1.set_index(dt1)
data2_dt = data2.set_index(dt2)

# get rid of redundant date column:
data1_dt.drop('fldDate', axis=1, inplace=True)
data2_dt.drop('fldDate', axis=1, inplace=True)

# BUILD CONTINUOUS TEMP SERIES:
# Grab current/max/min from data1 (1974-12-16 thru 2002-01-30)
CurTemp1 = data1_dt.CurTempMid[:'2002-01-30']
MaxTemp1 = data1_dt.MaxTempMid[:'2002-01-30']
MinTemp1 = data1_dt.MinTempMid[:'2002-01-30']

# Grab current/max/min from data2 (2002-01-31 thru 2016-01-03)
CurTemp2 = data2_dt.CurTempMid
MaxTemp2 = data2_dt.MaxTempMid
MinTemp2 = data2_dt.MinTempMid

# Define two corresponding series as a list
CurTemp_data = [CurTemp1, CurTemp2]
MaxTemp_data = [MaxTemp1, MaxTemp2]
MinTemp_data = [MinTemp1, MinTemp2]

# Concat to one continuous variable
CurTempMid = pd.concat(CurTemp_data)
MaxTempMid = pd.concat(MaxTemp_data)
MinTempMid = pd.concat(MinTemp_data)

# Count NaNs in temp data:
NaNcount_Cur = CurTempMid.isnull().sum()
fractNaN_Cur = float(NaNcount_Cur) / float(len(CurTempMid))
NaNcount_Max = MaxTempMid.isnull().sum()
fractNaN_Max = float(NaNcount_Max) / float(len(MaxTempMid))
NaNcount_Min = MinTempMid.isnull().sum()
fractNaN_Min = float(NaNcount_Min) / float(len(MinTempMid))

print '------------------------------'
print "%s NaN values in CurTempMid" %NaNcount_Cur
print "%f of data" %fractNaN_Cur 
print "%s NaN values in MaxTempMid" %NaNcount_Max
print "%f of data" %fractNaN_Max
print "%s NaN values in MinTempMid" %NaNcount_Min
print "%f of data" %fractNaN_Min 
print '------------------------------'

# Need continuous daily temp data to match length with snow data
# RESAMPLE (runs through summers)
CurTempMid_resample = CurTempMid.resample('1D')
MaxTempMid_resample = MaxTempMid.resample('1D')
MinTempMid_resample = MinTempMid.resample('1D')


# SNOW DATA:
# Count initial NaNs in snow data:
NaNcount_newsnow = data1_dt.NewSnowMid.isnull().sum()
fractNaN_newsnow = float(NaNcount_newsnow) / float(len(data1_dt.NewSnowMid))
NaNcount_SWE = data1_dt.SWEMid.isnull().sum()
fractNaN_SWE = float(NaNcount_SWE) / float(len(data1_dt.SWEMid))

print '------------------------------'
print "%s NaN values in NewSnowMid" %NaNcount_newsnow
print "%f of data" %fractNaN_newsnow 
print "%s NaN values in SWEMid" %NaNcount_SWE
print "%f of data" %fractNaN_SWE
print '------------------------------'

print 'Dropping any new snow entries <=1"'

NewSnowMid_countzero = data1_dt.NewSnowMid[data1_dt.NewSnowMid == 0.0]
NewSnowMid_drop = data1_dt.NewSnowMid[data1_dt.NewSnowMid > 1.0]

lessthaninch = len(data1_dt.NewSnowMid) - len(NewSnowMid_drop) - len(NewSnowMid_countzero)
totaldrop = len(data1_dt.NewSnowMid) - len(NewSnowMid_drop)

print "%s entries in NewSnowMid, >0 <=1" %lessthaninch
print "dropped %s entries from NewSnowMid (including 0)" %totaldrop

# Match length with temp data
NewSnowMid_resample = NewSnowMid_drop.resample('1D')
SWEMid_resample = data1_dt.SWEMid.resample('1D')

# Data length for both now at 14994

densityMid = SWEMid_resample / NewSnowMid_resample 
# any NaNs in NewSnowMid_resample should set corresponding densityMid to Nan

print 'Dropping outlier high density records'
densityMid = densityMid[densityMid < 0.25]

# Calc average temp:
temp_mean = (MaxTempMid_resample + MinTempMid_resample) / 2.

# Only select values where both temp_mean and densityMid are integers (non-NaN):
idx_mean = np.isfinite(temp_mean) & np.isfinite(densityMid)
idx_current = np.isfinite(CurTempMid_resample) & np.isfinite(densityMid)

x = temp_mean[idx_mean]
y = densityMid[idx_mean]

# -------------------------------------------------------------------------------------
# -------------------------------------------------------------------------------------

d = {'temp': x, 'dens': y}
df = pd.DataFrame(data=d)

x1 = pd.DataFrame({'temp': np.linspace(df.temp.min(), df.temp.max(), 200)})

poly_2 = smf.ols(formula='dens ~ 1 + temp + I(temp ** 2.0)', data=df).fit()
plt.plot(x, y, 'o', alpha=0.2)
plt.plot(x1.temp, poly_2.predict(x1), 'r-', 
         label='2nd order poly fit, $R^2$=%.2f' % poly_2.rsquared, 
         alpha=0.9)
plt.xlim((-10, 50))
plt.ylim((0, 0.25))
plt.xlabel('mean air temp')
plt.ylabel('density')
plt.legend(loc="upper left")


# with quantile regression

# Least Absolute Deviation
# The LAD model is a special case of quantile regression where q=0.5

mod = smf.quantreg('dens ~ temp + I(temp ** 2.0)', df)
res = mod.fit(q=.5)
print(res.summary())

# Quantile regression for 5 quantiles

quantiles = [.05, .25, .50, .75, .95]

# get all result instances in a list
res_all = [mod.fit(q=q) for q in quantiles]

res_ols = smf.ols('dens ~ temp + I(temp ** 2.0)', df).fit()


plt.figure()

# create x for prediction
x_p = np.linspace(df.temp.min(), df.temp.max(), 50)
df_p = pd.DataFrame({'temp': x_p})

for qm, res in zip(quantiles, res_all):
    # get prediction for the model and plot
    # here we use a dict which works the same way as the df in ols
    plt.plot(x_p, res.predict({'temp': x_p}), linestyle='--', lw=1, 
             color='k', label='q=%.2F' % qm, zorder=2)
    
y_ols_predicted = res_ols.predict(df_p)
plt.plot(x_p, y_ols_predicted, color='red', zorder=1)
#plt.scatter(df.temp, df.dens, alpha=.2)
plt.plot(df.temp, df.dens, 'o', alpha=.2, zorder=0)
plt.xlim((-10, 50))
plt.ylim((0, 0.25))
#plt.legend(loc="upper center")
plt.xlabel('mean air temp')
plt.ylabel('density')
plt.title('')
plt.show()