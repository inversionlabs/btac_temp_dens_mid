#!/usr/bin/env python

# Script to analyze air temp and density data
# Patrick Wright, Inversion Labs
# Job: BTAC
# January, 2016

import numpy as np
import scipy as sp
from scipy import stats
import scipy.stats as stats
import pylab 
import pandas as pd
from IPython import embed
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.mlab as mlab
import statsmodels.api as sm
from statsmodels.sandbox.regression.predstd import wls_prediction_std
import statsmodels.formula.api as smf

# --------------------------------------------------------------------------
def histoplot_normal(data,titlestring):
	num_bins= 100
	fig10 = plt.figure(figsize=(10,7))
	ax = fig10.add_subplot(111)
	n, bins, patches = plt.hist(data, num_bins, normed=1, facecolor='blue', alpha = 0.5)
	y = mlab.normpdf(bins, np.mean(data), np.std(data))
	plt.plot(bins, y, 'k--')
	plt.axvline(x=np.median(data), ymin=0, ymax=180, linewidth=1, color='r')
	plt.axvline(x=np.mean(data), ymin=0, ymax=180, linewidth=1, color='k')
	ax.set_title(titlestring)
	ax.set_xlabel('residuals')
	ax.set_ylabel('probability density')
	
def histoplot(data,titlestring):
	num_bins=(2340 / 10.) #2329
	fig10 = plt.figure(figsize=(10,7))
	ax = fig10.add_subplot(111)
	n, bins, patches = plt.hist(data, num_bins, normed=1, facecolor='blue', alpha = 0.5)
	#plt.axvline(x=np.median(data), ymin=0, ymax=180, linewidth=1, color='r')
	#plt.axvline(x=np.mean(data), ymin=0, ymax=180, linewidth=1, color='k')
	ax.set_title(titlestring)
	ax.set_xlabel('density')
	ax.set_ylabel('# measurements / 10')

# --------------------------------------------------------------------------
print "Processing datafiles..."

# Define directory paths:
fpath_1='dbo.HistoricalWeatherTable_20160103.csv' # historical data
fpath_2='dbo.Teton_AM_Forecast_Data.csv' # temps since 2002

# Read in data:
data1 = pd.read_csv(fpath_1, sep=',')
data2 = pd.read_csv(fpath_2, sep=',')

# Drop last entry of data1 (NaN for date, no data)
data1 = data1[:-1]

# Convert datetime strings to a Pandas datetime object
timestamp1 = data1['fldDate']
dt1 = pd.to_datetime(timestamp1, infer_datetime_format=True)
timestamp2 = data2['fldDate']
dt2 = pd.to_datetime(timestamp2, infer_datetime_format=True)

# set index of dataframes to datetime
data1_dt = data1.set_index(dt1)
data2_dt = data2.set_index(dt2)

# get rid of redundant date column:
data1_dt.drop('fldDate', axis=1, inplace=True)
data2_dt.drop('fldDate', axis=1, inplace=True)

# BUILD CONTINUOUS TEMP SERIES:
# Grab current/max/min from data1 (1974-12-16 thru 2002-01-30)
CurTemp1 = data1_dt.CurTempMid[:'2002-01-30']
MaxTemp1 = data1_dt.MaxTempMid[:'2002-01-30']
MinTemp1 = data1_dt.MinTempMid[:'2002-01-30']

# Grab current/max/min from data2 (2002-01-31 thru 2016-01-03)
CurTemp2 = data2_dt.CurTempMid
MaxTemp2 = data2_dt.MaxTempMid
MinTemp2 = data2_dt.MinTempMid

# Define two corresponding series as a list
CurTemp_data = [CurTemp1, CurTemp2]
MaxTemp_data = [MaxTemp1, MaxTemp2]
MinTemp_data = [MinTemp1, MinTemp2]

# Concat to one continuous variable
CurTempMid = pd.concat(CurTemp_data)
MaxTempMid = pd.concat(MaxTemp_data)
MinTempMid = pd.concat(MinTemp_data)

# Count NaNs in temp data:
NaNcount_Cur = CurTempMid.isnull().sum()
fractNaN_Cur = float(NaNcount_Cur) / float(len(CurTempMid))
NaNcount_Max = MaxTempMid.isnull().sum()
fractNaN_Max = float(NaNcount_Max) / float(len(MaxTempMid))
NaNcount_Min = MinTempMid.isnull().sum()
fractNaN_Min = float(NaNcount_Min) / float(len(MinTempMid))

print '------------------------------'
print "%s NaN values in CurTempMid" %NaNcount_Cur
print "%f of data" %fractNaN_Cur 
print "%s NaN values in MaxTempMid" %NaNcount_Max
print "%f of data" %fractNaN_Max
print "%s NaN values in MinTempMid" %NaNcount_Min
print "%f of data" %fractNaN_Min 
print '------------------------------'

# Need continuous daily temp data to match length with snow data
# RESAMPLE (runs through summers)
CurTempMid_resample = CurTempMid.resample('1D')
MaxTempMid_resample = MaxTempMid.resample('1D')
MinTempMid_resample = MinTempMid.resample('1D')


# SNOW DATA:
# Count initial NaNs in snow data:
NaNcount_newsnow = data1_dt.NewSnowMid.isnull().sum()
fractNaN_newsnow = float(NaNcount_newsnow) / float(len(data1_dt.NewSnowMid))
NaNcount_SWE = data1_dt.SWEMid.isnull().sum()
fractNaN_SWE = float(NaNcount_SWE) / float(len(data1_dt.SWEMid))

print '------------------------------'
print "%s NaN values in NewSnowMid" %NaNcount_newsnow
print "%f of data" %fractNaN_newsnow 
print "%s NaN values in SWEMid" %NaNcount_SWE
print "%f of data" %fractNaN_SWE
print '------------------------------'

print 'Dropping any new snow entries <=1"'

NewSnowMid_countzero = data1_dt.NewSnowMid[data1_dt.NewSnowMid == 0.0]
NewSnowMid_drop = data1_dt.NewSnowMid[data1_dt.NewSnowMid > 1.0]

lessthaninch = len(data1_dt.NewSnowMid) - len(NewSnowMid_drop) - len(NewSnowMid_countzero)
totaldrop = len(data1_dt.NewSnowMid) - len(NewSnowMid_drop)

print "%s entries in NewSnowMid, >0 <=1" %lessthaninch
print "dropped %s entries from NewSnowMid (including 0)" %totaldrop

# Match length with temp data
NewSnowMid_resample = NewSnowMid_drop.resample('1D')
SWEMid_resample = data1_dt.SWEMid.resample('1D')

# Data length for both now at 14994

densityMid = SWEMid_resample / NewSnowMid_resample 
# any NaNs in NewSnowMid_resample should set corresponding densityMid to Nan

print 'Dropping outlier high density records'
densityMid = densityMid[densityMid < 0.25]

# Calc average temp:
temp_mean = (MaxTempMid_resample + MinTempMid_resample) / 2.


# --------------------------------------------------------------------------
# --------------------------------------------------------------------------

# Calc a polynomial best-fit line, using basic numpy example:

# Only select values where both temp_mean and densityMid are integers (non-NaN):
idx_mean = np.isfinite(temp_mean) & np.isfinite(densityMid)
idx_current = np.isfinite(CurTempMid_resample) & np.isfinite(densityMid)

# 2nd-order polynomial fit:
p2_mean_full = np.polyfit(temp_mean[idx_mean], densityMid[idx_mean], 2, full=True) # all terms

p2_mean = np.poly1d(np.polyfit(temp_mean[idx_mean], densityMid[idx_mean], 2)) #poly1d returns equation using coeff's from polyfit
p2_current = np.poly1d(np.polyfit(CurTempMid[idx_current], densityMid[idx_current], 2))

xp = np.linspace(-20,45,600) # make an x-array for fit line. arbitrary length.

#-----------------------------------------------------------------------------
# From: http://stackoverflow.com/questions/3054191/converting-numpy-lstsq-residual-value-to-r2

x = temp_mean[idx_mean]
y = densityMid[idx_mean]
n = np.max(x.shape)    # the number of observations
X = np.vstack([np.ones(n), x]).T

print ''
print 'n=%s' % n
print ''

model, resid = np.polyfit(x,y,1,full=True)[:2]
r2 = 1 - resid / (y.size * y.var())
print 'r2 linear: %s' %r2
r2_equiv = (np.corrcoef(x,y)[0,1])**2. # equiv to above

std_error = np.sqrt(resid/(n-2)) # equiv to method below
print 'std_error linear %s:' % std_error

model, resid = np.polyfit(x,y,2,full=True)[:2]
r2 = 1 - resid / (y.size * y.var())
print 'r2 2nd-order poly: %s' %r2

resid_alt = np.sum((np.polyval(np.polyfit(x, y, 2), x) - y)**2) # equiv. to resid

#resid_array = (np.polyval(np.polyfit(x, y, 2), x) - y) # plot this!
resid_array = (y - np.polyval(np.polyfit(x, y, 2), x)) # plot this! No difference.... makes more sense on plot.
sorted_resid_array = np.sort(resid_array,axis=0)

p,cov = np.polyfit(x,y,2,cov=True) # the covariance matrix
variance = np.diagonal(cov)
SE = np.sqrt(variance) # standard error of coeff's

std_error_2 = np.sqrt(resid/(n-3))
print 'std_error 2nd-order poly %s:' % std_error_2


#-----------------------------------------------------------------------------
# From: http://modelling3e4.connectmv.com/wiki/Software_tutorial/Least_squares_modelling_(linear_regression)
# Example is for 1st-order linear only.
# Equivalent functionality to code above.
# (not used in plots)

x = temp_mean[idx_mean]
y = densityMid[idx_mean]
n = np.max(x.shape)    # the number of observations
X = np.vstack([np.ones(n), x]).T

a = np.linalg.lstsq(X, y)[0]
resids = y - np.dot(X,a)       # e = y - Xa; 
RSS = sum(resids**2)           # sum of residual squares; equiv. to 'resid' in polyfit
TSS = sum((y - np.mean(y))**2) # total sum of squares
R2 = 1 - RSS/TSS
#print 'r2 linear alt: %s' %R2

std_error = np.sqrt(RSS/(n-len(a)))
std_y = np.sqrt(TSS/(n-1)) 
R2_adj = 1 - (std_error/std_y)**2

print 'std error 1st order alt %s' %std_error


#-----------------------------------------------------------------------------
# From: http://www.datarobot.com/blog/multiple-regression-using-statsmodels/

d = {'temp': x, 'dens': y}
df = pd.DataFrame(data=d)

plt.figure(figsize=(6 * 1.618, 6))
plt.scatter(x,y, s=10, alpha=0.3)
plt.xlabel('mean air temp (F)')
plt.ylabel('density')

# points linearly spaced on temp
x1 = pd.DataFrame({'temp': np.linspace(df.temp.min(), df.temp.max(), 100)})

# 1st order polynomial
poly_1 = smf.ols(formula='dens ~ 1 + temp', data=df).fit()
plt.plot(x1.temp, poly_1.predict(x1), 'b-', label='1st order poly fit, $R^2$=%.2f' % poly_1.rsquared, 
         alpha=0.9)
print 'POLY1 SUMMARY: %s' % poly_1.summary()

# 2nd order polynomial
poly_2 = smf.ols(formula='dens ~ 1 + temp + I(temp ** 2.0)', data=df).fit()
plt.plot(x1.temp, poly_2.predict(x1), 'g-', label='2nd order poly fit, $R^2$=%.2f' % poly_2.rsquared, 
         alpha=0.9)
print 'POLY2 SUMMARY: %s' % poly_2.summary()

# 3rd order polynomial
poly_3 = smf.ols(formula='dens ~ 1 + temp + I(temp ** 2.0) + I(temp ** 3.0)', data=df).fit()
plt.plot(x1.temp, poly_3.predict(x1), 'r-', alpha=0.9,
         label='3rd order poly fit, $R^2$=%.2f' % poly_3.rsquared)
         

plt.legend()

#-----------------------------------------------------------------------------
# CI and PI for 2nd order poly:
# adapted from: http://stackoverflow.com/questions/34998772/plotting-confidence-and-prediction-intervals-with-repeated-entries

d = {'temp': x, 'dens': y}
df = pd.DataFrame(data=d)

plt.figure(figsize=(6 * 1.618, 6))
plt.scatter(x,y, s=10, alpha=0.3)
plt.xlabel('mean air temp (F)')
plt.ylabel('density')

# points linearly spaced for predictor variable
x1 = pd.DataFrame({'temp': np.linspace(df.temp.min(), df.temp.max(), 100)})

# 2nd order polynomial
poly_2 = smf.ols(formula='dens ~ 1 + temp + I(temp ** 2.0)', data=df).fit()
plt.plot(x1, poly_2.predict(x1), 'k-', label='2nd order poly fit, $R^2$=%.2f' % poly_2.rsquared, alpha=0.9)

prstd, iv_l, iv_u = wls_prediction_std(poly_2)

from statsmodels.stats.outliers_influence import summary_table

st, data, ss2 = summary_table(poly_2, alpha=0.05)

fittedvalues = data[:,2]
predict_mean_se  = data[:,3]
predict_mean_ci_low, predict_mean_ci_upp = data[:,4:6].T
predict_ci_low, predict_ci_upp = data[:,6:8].T

# check we got the right things
print np.max(np.abs(poly_2.fittedvalues - fittedvalues))
print np.max(np.abs(iv_l - predict_ci_low))
print np.max(np.abs(iv_u - predict_ci_upp))

data_intervals = {'temp': x, 'predict_low': predict_ci_low, 'predict_upp': predict_ci_upp, 'conf_low': predict_mean_ci_low, 'conf_high': predict_mean_ci_upp}
df_intervals = pd.DataFrame(data=data_intervals)

df_intervals_sort = df_intervals.sort(columns='temp')

#embed()

plt.plot(df_intervals_sort.temp, df_intervals_sort.predict_low, color='r', linestyle='--', label='95% prediction interval')
plt.plot(df_intervals_sort.temp, df_intervals_sort.predict_upp, color='r', linestyle='--', label='')
plt.plot(df_intervals_sort.temp, df_intervals_sort.conf_low, color='c', linestyle='--', label='95% confidence interval')
plt.plot(df_intervals_sort.temp, df_intervals_sort.conf_high, color='c', linestyle='--', label='')

plt.legend()

#plt.show()
#embed()         


#------------------------------------------------------------------------------
#------------------------------------------------------------------------------

#TEMPS:
#fig1 = plt.figure()
#ax1 = fig1.add_subplot(111)
#ax1.scatter(temp_mean.index, temp_mean, label='mean temp') 
#ax1.set_ylabel('Air Temp (F)')
#plt.axhline(y=32.0, color='r', linestyle='dashed')
#ax1.set_title('Mean Air Temp (F) ((Max + Min) / 2)')
##ax1.legend()
#fig1.tight_layout()  # Make the figure use all available whitespace


## BASIC 2nd ORDER PLOT
#fig2 = plt.figure()
#ax1 = fig2.add_subplot(111)
#ax1.scatter(df.temp, df.dens, alpha=0.18, s=50)
#ax1.plot(xp, p2_mean(xp), '-', color='r')
#ax1.set_ylabel(r'Density ($\rho_s / \rho_w$)')
#ax1.set_xlabel('mean air temp (F) ((max + min) / 2)')
#ax1.set_title('')
#ax1.set_xlim([-20, 45])
#ax1.set_ylim([0, 0.50])
##ax1.legend()
#fig2.tight_layout()  # Make the figure use all available whitespace

## DENSITY VS. 5 AM TEMPS
#fig3 = plt.figure()
#ax1 = fig3.add_subplot(111)
#ax1.scatter(CurTempMid_resample, densityMid)
#ax1.plot(xp, p2_current(xp), '-', color='r')
#ax1.set_ylabel(r'Density ($\rho_s / \rho_w$)')
#ax1.set_xlabel('5am Air Temp (F)')
#ax1.set_title('')
#ax1.set_xlim([-20, 45])
#ax1.set_ylim([0, 0.50])
##ax1.legend()
#fig3.tight_layout()  # Make the figure use all available whitespace

# RESIDUALS:
fig4 = plt.figure()
ax1 = fig4.add_subplot(111)
ax1.scatter(x, resid_array)
plt.axhline(y=0.0, color='k', linestyle='dashed')
ax1.set_ylabel('residuals (density)')
ax1.set_xlabel('mean air temp (F)')
ax1.set_title('')
ax1.set_xlim([-20, 45])
ax1.set_ylim([-0.15, 0.12])
fig4.tight_layout()  # Make the figure use all available whitespace

fig10 = plt.figure()
measurements = resid_array   
stats.probplot(measurements, dist="norm", plot=pylab)

histoplot_normal(resid_array, 'normal prob plot')

histoplot(df.dens, 'density distribution')

## ORDERED RESIDUALS:
#ordered_x = np.linspace(1,2340,num=2340)
#fig5 = plt.figure()
#ax1 = fig5.add_subplot(111)
#ax1.scatter(ordered_x, sorted_resid_array)
#ax1.set_ylabel('sorted residuals (density)')
#ax1.set_xlabel('')
#ax1.set_title('')
##ax1.set_xlim([-20, 45])
#ax1.set_ylim([-0.15, 0.12])
#fig5.tight_layout()  # Make the figure use all available whitespace

plt.show()

print "Done"

#-----------------------------------------------------------------------------
# EXTRA
#-----------------------------------------------------------------------------

## From: http://www.datarobot.com/blog/ordinary-least-squares-in-python/
## LINEAR WITH AND WITHOUT INTERCEPT

## OLS FIT, w/INTERCEPT:
#d = {'temp': x, 'dens': y}
#df = pd.DataFrame(data=d)

#Y = df.dens # response variable
#X = df.temp # predictor variable
#X = sm.add_constant(X) # add constant term to predictor (col of 1's)

##embed()

#est = sm.OLS(Y, X)

#est = est.fit()
##print est.summary()

#X_prime = np.linspace(X.temp.min(), X.temp.max(), 100)[:, np.newaxis]
#X_prime = sm.add_constant(X_prime)  # add constant as before

## Now we calculate the predicted values
#y_hat = est.predict(X_prime)

#plt.scatter(X.temp, y, alpha=0.3)  # Plot the raw data
#plt.xlabel("temp")
#plt.ylabel("density")
#plt.plot(X_prime[:, 1], y_hat, 'r', alpha=0.9)  # Add the regression line, colored in red

## NO INTERCEPT:
#est_no_int = smf.ols(formula='dens ~ temp - 1', data=df).fit()

## print est_no_int.summary()

#X_prime_1 = pd.DataFrame({'temp': np.linspace(X.temp.min(), X.temp.max(), 100)})
#X_prime_1 = sm.add_constant(X_prime_1)  # add constant as we did before

#y_hat_int = est.predict(X_prime_1)
#y_hat_no_int = est_no_int.predict(X_prime_1)

#fig = plt.figure(figsize=(8,4))
#splt = plt.subplot(121)

#splt.scatter(X.temp, y, alpha=0.3)  # Plot the raw data
#plt.xlabel("temp")
#plt.ylabel("density")
#plt.title("with intercept")
#splt.plot(X_prime[:, 1], y_hat_int, 'r', alpha=0.9)  # Add the regression line, colored in red

#splt = plt.subplot(122)
#splt.scatter(X.temp, y, alpha=0.3)  # Plot the raw data
#plt.xlabel("temp")
#plt.title("without intercept")
#splt.plot(X_prime[:, 1], y_hat_no_int, 'r', alpha=0.9)  # Add the regression line, colored in red

#-----------------------------------------------------------------------------